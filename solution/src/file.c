#include "file.h"
#include "bmp_image.h"
#include<stdlib.h>

uint64_t Padding(uint32_t width) {
 return (4 - 3 * width % 4) % 4;
}


enum write_status to_bmp(FILE* out, struct image const* img) {
	const uint16_t bfType = 0x4D42; 
	struct bmp_header Header;
	Header.biWidth = img->width;
	Header.bfType = bfType;
	Header.biHeight = img->height;
	Header.bfileSize = (sizeof(struct bmp_header)) +
		(sizeof(struct pixel) * img->height * img->width);
	Header.biSizeImage = sizeof(struct pixel) *
		img->width * img->height;
	Header.bOffBits = sizeof(struct bmp_header);
	Header.biBitCount = 24;
	Header.biSize = 40;
	Header.bfReserved = 0;
	Header.biPlanes = 1;
	Header.biCompression = 0;
	Header.biXPelsPerMeter = 0;
	Header.biYPelsPerMeter = 0;
	Header.biClrUsed = 0;
	Header.biClrImportant = 0;
	if (fwrite(&Header, sizeof(struct bmp_header), 1, out)!=1) {
		return WRITE_ERROR;
	}
	uint64_t padding = Padding(img ->width);

	int8_t a[3]={0};
	for (size_t i = 0; i < img->height; i++) {
		fwrite(img->data[i], sizeof(struct pixel), img->width, out);
		if (ferror(out) != 0) {
			return WRITE_ERROR;
		}
		if (padding != 0) {
			fwrite(a, sizeof(int8_t), padding, out);
		}
	}
	return WRITE_OK;
}


enum read_status from_bmp(FILE* in, struct image* img) {
	struct bmp_header Header;
	if (fread(&Header, sizeof(struct bmp_header), 1, in) == 0) {
		return READ_INVALID_HEADER;
	}
	if (Header.bfType != 19778)
	{
		return READ_INVALID_SIGNATURE;
	}
	img->height=Header.biHeight;
	img->width=Header.biWidth;
	if (img->height == 0 || img->width == 0)
	{
		return READ_INVALID_HEADER;
	}
	img->data = malloc(sizeof(struct pixel*)* Header.biHeight);
	if (img->data == NULL) {
		return LESS_MEMORRY;
	}
	for (uint64_t i = 0; i < img->height; i++)
	{
		img->data[i] = malloc(sizeof(struct pixel)* img->width);
		if (img->data[i] == NULL) {
			for (uint64_t j = i - 1; j != 0; j--) {
				free(img->data[i]);
			}
			free(img->data[0]);
			free(img->data);
			return LESS_MEMORRY;
		
		}
	}
	uint64_t padding = Padding(img->width);
	if(Header.bOffBits < sizeof(struct bmp_header))
		return READ_INVALID_HEADER;
	if (fseek(in, Header.bOffBits, SEEK_SET) != 0)
	{
		return READ_INVALID_HEADER;
	}
	for (uint64_t i = 0; i < img->height; i++) {
		fread(img->data[i], sizeof(struct pixel), img->width,in);
		if (ferror(in)!=0) {
			return READ_INVALID_BITS;
		}
		if (padding != 0) {
			fseek(in, (long)padding, SEEK_CUR);
		}
	}
	return READ_OK;
}



