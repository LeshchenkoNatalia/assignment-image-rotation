#include "file.h"
#include "bmp_image.h"
#include<stdlib.h>


int create_image(struct image*const source, struct image**dest)
{
	*dest = malloc(sizeof(struct image) * 1);
	if (*dest == NULL) {
		fprintf(stderr, "Lack of memory error");
		return 1;
	}
	**dest = (struct image){ 0 };
	(*dest)->height = source->width;
	(*dest)->width = source->height;
	(*dest)->data = calloc(sizeof(struct pixel*), (*dest)->height);
	if ((*dest)->data == NULL) {
		free(*dest);
		fprintf(stderr, "Lack of memory error");
		return 1;
	}
	for (uint64_t i = 0; i < (*dest)->height; i++)
	{
		(*dest)->data[i] = calloc(sizeof(struct pixel), (*dest)->width);
		if ((*dest)->data[i] == NULL) {
			for (uint64_t j = i - 1; j != 0; j--) {
				free((*dest)->data[i]);
			}
			free((*dest)->data[0]);
			free((*dest)->data);
			free(*dest);
			fprintf(stderr, "Lack of memory error");
			return 1;
		}
	}
	return 0;
}

struct image* rotate(struct image* const source) {
	struct image* rotateImage=NULL;
	if (create_image(source, &rotateImage))
		return NULL;
	for (uint64_t i = 0; i < rotateImage->height; i++)
		for (uint64_t j = 0; j < rotateImage->width; j++)
		{
			rotateImage->data[i][j] = source->data[j][i];
		}
	for (uint64_t i = 0; i < rotateImage->height; ++i) {
		for (uint64_t j = 0; j < rotateImage->width / 2; ++j) {
			struct pixel temp = rotateImage->data[i][j];
			rotateImage->data[i][j] = rotateImage->data[i][rotateImage->width - j - 1];
			rotateImage->data[i][rotateImage->width - j - 1] = temp;
		}
	}
	return rotateImage;
}
