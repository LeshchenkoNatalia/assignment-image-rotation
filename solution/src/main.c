#include "./file.h"
#include "./bmp_image.h"
#include "./errors.h"
#include "utils.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char* argv[]) {
	
	if (argc != 3) {
		fprintf(stderr, "Use: ./image-transformer <source-image> <transformed-image>");
		return ARGS_ERROR;
	}
	FILE* src_file = fopen(argv[1], "rb");
	
	
	if (src_file == NULL) {
		fprintf(stderr, "File opening error");
		return SRC_FILE_ERROR;
	}
	struct image* Image;
	Image = malloc(sizeof(struct image) * 1);
	if (Image == NULL) {
		fprintf(stderr, "Lack of memory error");
		return MEMORY_ERROR;
	}
	*Image = (struct image){ 0 };
	enum read_status read_status;
	read_status = from_bmp(src_file, Image);
	if (read_status != READ_OK) {
		fprintf(stderr, "Invalid BMP file");
		return READ_FILE_ERROR;
	}
	struct image* rotateImage;
	rotateImage = rotate(Image);
	if (rotateImage == NULL) {
		return ROTATE_ERROR;
	}
	FILE* out_file = fopen(argv[2], "wb");
	enum write_status write_status;
	write_status = to_bmp(out_file, rotateImage);
	if (write_status != WRITE_OK) {
		fprintf(stderr, "File recording error");
		return WRITE_FILE_ERROR;
	}



	

	fclose(out_file);
	fclose(src_file);
	for (uint64_t i = 0; i < Image->height; i++) {
		free(Image->data[i]);
	}
	free(Image->data);
	free(Image);
	for (uint64_t i = 0; i < rotateImage->height; i++) {
		free(rotateImage->data[i]);
	}
	free(rotateImage->data);
	free(rotateImage);



}





